package asc.ps.voda.supporter.operator.activities.main;

import org.json.JSONException;
import org.json.JSONObject;

public class PresenterMainCustomer implements ContractMainCustomer.presenter {
    private final ModelMainCustomer model;
    private final ContractMainCustomer.view view;

    public PresenterMainCustomer(ContractMainCustomer.view view) {
        this.model = new ModelMainCustomer(this);
        this.view = view;
    }

    @Override
    public void doServerThings() {
//        model.getHealthDocuments();
//        model.getWallet();
//        model.getMyReserves();
//        model.getPosts();
//        model.getPrescriptions();
//        model.getHistory();
    }

    @Override
    public void onServerResponse(int type,String s) {
        switch (type) {
            case ModelMainCustomer.TYPE_POSTS:
                /*try {
                    JSONObject object = new JSONObject(s);
                    JSONArray data = object.getJSONArray("data");
                    ArrayList<PostItem> postItems = new ArrayList<>();
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject item = data.getJSONObject(i);
                        PostItem postItem = new PostItem();
                        postItem.setTitle(item.getString("title"));
                        postItem.setDsc(item.getString("dsc"));
                        postItem.setImg2(item.getString("img2"));
                        postItems.add(postItem);
                    }
                    view.postsResponse(postItems);
                } catch (JSONException e) {
                    e.printStackTrace();
                    view.jsonParseError();
                }*/
                break;
            default:
                view.serverResponseError();
        }
    }

    @Override
    public void onServerError(int type,String error) {
        view.serverResponseError(error);
    }

    @Override
    public void onServerError(int type,String s, int statusCode) {
        switch (type) {
            case ModelMainCustomer.TYPE_HEALTH_DOCUMENTS:
                view.serverResponseError(s);
                break;
            case ModelMainCustomer.TYPE_WALLET:
                view.serverResponseError(s);
                break;
            default:
                view.serverResponseError(s);
        }
    }

    @Override
    public void onServerError(int type) {
        view.serverResponseError();
    }

    /*private void parseProjectsResponse(String s) {
        try {
            JSONObject object = new JSONObject(s);
            JSONObject status = object.getJSONObject("status");
            int statusCode = status.getInt("code");
            if (statusCode == 200) {
                ArrayList<ProjectItem> projectItems = new ArrayList<>();
                JSONArray data = object.getJSONArray("data");
                for (int i = 0; i < data.length(); i++) {
                    JSONObject dataItem = data.getJSONObject(i);
                    ProjectItem projectItem = new ProjectItem();
                    projectItem.setId(dataItem.getInt("id"));
                    projectItem.setName(dataItem.getString("name"));
                    projectItem.setAddress(dataItem.getString("address"));
                    projectItem.setPhone(dataItem.getString("phone"));
                    projectItem.setType(dataItem.getInt("type"));
                    projectItem.setManagerName(dataItem.getString("project_manager_name"));
                    projectItem.setManagerPhone(dataItem.getString("project_manager_phone_number"));
                    projectItem.setPremium(dataItem.getInt("premium") != 0);
                    projectItem.setLat(dataItem.getDouble("lat"));
                    projectItem.setLng(dataItem.getDouble("lng"));
                    projectItems.add(projectItem);
                }
                view.projectsResponse(projectItems);
            } else {
                view.serverResponseError();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            view.jsonParseError();
        }
    }

    private void parseSectionsResponse(String s) {
        try {
            JSONObject object = new JSONObject(s);
            JSONObject status = object.getJSONObject("status");
            int statusCode = status.getInt("code");
            if (statusCode == 200) {
                ArrayList<SectionItem> sectionItems = new ArrayList<>();
                JSONArray data = object.getJSONArray("data");
                for (int i = 0; i < data.length(); i++) {
                    JSONObject dataItem = data.getJSONObject(i);
                    SectionItem sectionItem = new SectionItem();
                    sectionItem.setId(dataItem.getInt("id"));
                    sectionItem.setName(dataItem.getString("name"));
                    sectionItem.setAddress(dataItem.getString("address"));
                    sectionItem.setPhone(dataItem.getString("phone"));
                    sectionItem.setType(dataItem.getInt("type"));
                    sectionItem.setPremium(dataItem.getInt("premium") != 0);
                    sectionItem.setLat(dataItem.getDouble("lat"));
                    sectionItem.setLng(dataItem.getDouble("lng"));
                    sectionItem.setProjectId(dataItem.getInt("project_id"));
                    sectionItem.setProjectName(dataItem.getString("project_name"));
                    sectionItem.setProjectManagerName(dataItem.getString("project_manager_name"));
                    sectionItem.setProjectManagerPhoneNumber(dataItem.getString("project_manager_phone_number"));
                    sectionItems.add(sectionItem);
                }
                view.sectionsResponse(sectionItems);
            } else {
                view.serverResponseError();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            view.jsonParseError();
        }
    }

    private void parsePhasesResponse(String s) {
        try {
            JSONObject object = new JSONObject(s);
            JSONObject status = object.getJSONObject("status");
            int statusCode = status.getInt("code");
            if (statusCode == 200) {
                ArrayList<PhaseItem> phaseItems = new ArrayList<>();
                JSONArray data = object.getJSONArray("data");
                for (int i = 0; i < data.length(); i++) {
                    JSONObject dataItem = data.getJSONObject(i);
                    PhaseItem phaseItem = new PhaseItem();
                    phaseItem.setId(dataItem.getInt("id"));
                    phaseItem.setName(dataItem.getString("name"));
                    phaseItem.setAddress(dataItem.getString("address"));
                    phaseItem.setPhone(dataItem.getString("phone"));
                    phaseItem.setType(dataItem.getInt("type"));
                    phaseItem.setPremium(dataItem.getInt("premium") != 0);
                    phaseItem.setLat(dataItem.getDouble("lat"));
                    phaseItem.setLng(dataItem.getDouble("lng"));
                    phaseItem.setProjectId(dataItem.getInt("project_id"));
                    phaseItem.setProjectName(dataItem.getString("project_name"));
                    phaseItem.setProjectManagerName(dataItem.getString("project_manager_name"));
                    phaseItem.setProjectManagerPhoneNumber(dataItem.getString("project_manager_phone_number"));
                    phaseItem.setSectionId(dataItem.getInt("section_id"));
                    phaseItem.setSectionName(dataItem.getString("section_name"));
                    phaseItem.setSectionManagerName(dataItem.getString("section_manager_name"));
                    phaseItem.setSectionManagerPhoneNumber(dataItem.getString("section_manager_phone_number"));
                    phaseItems.add(phaseItem);
                }
                view.phasesResponse(phaseItems);
            } else {
                view.serverResponseError();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            view.jsonParseError();
        }
    }

    private void parsePropertiesResponse(String s) {
        try {
            JSONObject object = new JSONObject(s);
            JSONObject status = object.getJSONObject("status");
            int statusCode = status.getInt("code");
            if (statusCode == 200) {
                ArrayList<PropertyItem> propertyItems = new ArrayList<>();
                JSONArray data = object.getJSONArray("data");
                for (int i = 0; i < data.length(); i++) {
                    JSONObject dataItem = data.getJSONObject(i);
                    PropertyItem propertyItem = new PropertyItem();
                    propertyItem.setId(dataItem.getInt("id"));
                    propertyItem.setName(dataItem.getString("name"));
                    propertyItem.setAddress(dataItem.getString("address"));
                    propertyItem.setPhone(dataItem.getString("phone"));
                    propertyItem.setType(dataItem.getInt("type"));
                    propertyItem.setProjectId(dataItem.getInt("project_id"));
                    propertyItem.setProjectName(dataItem.getString("project_name"));
                    propertyItem.setProjectManagerName(dataItem.getString("project_manager_name"));
                    propertyItem.setProjectManagerPhoneNumber(dataItem.getString("project_manager_phone_number"));
                    propertyItem.setSectionId(dataItem.getInt("section_id"));
                    propertyItem.setSectionName(dataItem.getString("section_name"));
                    propertyItem.setSectionManagerName(dataItem.getString("section_manager_name"));
                    propertyItem.setSectionManagerPhoneNumber(dataItem.getString("section_manager_phone_number"));
                    propertyItem.setPhaseId(dataItem.getInt("phase_id"));
                    propertyItem.setPhaseName(dataItem.getString("phase_name"));
                    propertyItem.setPhaseManagerName(dataItem.getString("phase_manager_name"));
                    propertyItem.setPhaseManagerPhoneNumber(dataItem.getString("phase_manager_phone_number"));
                    propertyItem.setSitterId(dataItem.getInt("sitter_id"));
                    propertyItem.setSitterName(dataItem.getString("sitter_name"));
                    propertyItem.setSitterPhoneNumber(dataItem.getString("sitter_phone_number"));
                    propertyItem.setOwnerId(dataItem.getInt("owner_id"));
                    propertyItem.setOwnerName(dataItem.getString("owner_name"));
                    propertyItem.setOwnerPhoneNumber(dataItem.getString("owner_phone_number"));
                    propertyItems.add(propertyItem);
                }
                view.propertiesResponse(propertyItems);
            } else {
                view.serverResponseError();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            view.jsonParseError();
        }
    }

    private void parseInvoicesResponse(String s) {
        try {
            JSONObject object = new JSONObject(s);
            JSONObject status = object.getJSONObject("status");
            int statusCode = status.getInt("code");
            if (statusCode == 200) {
                ArrayList<InvoiceItem> invoiceItems = new ArrayList<>();
                JSONArray data = object.getJSONArray("data");
                for (int i = 0; i < data.length(); i++) {
                    JSONObject dataItem = data.getJSONObject(i);
                    InvoiceItem invoiceItem = new InvoiceItem();
                    invoiceItem.setInvoiceId(dataItem.getInt("invoice_id"));
                    invoiceItem.setPropertyId(dataItem.getInt("property_id"));
                    invoiceItem.setPhaseId(dataItem.getInt("phase_id"));
                    invoiceItem.setSectionId(dataItem.getInt("section_id"));
                    invoiceItem.setProjectId(dataItem.getInt("project_id"));
                    if (dataItem.has("customer_id")) {
                        invoiceItem.setCustomerId(dataItem.getInt("customer_id"));
                        invoiceItem.setCustomerName(dataItem.getString("customer_name"));
                        invoiceItem.setCustomerPhone(dataItem.getString("customer_phone"));
                    }
                    invoiceItem.setPropertyName(dataItem.getString("property_name"));
                    invoiceItem.setPhaseName(dataItem.getString("phase_name"));
                    invoiceItem.setSectionName(dataItem.getString("section_name"));
                    invoiceItem.setProjectName(dataItem.getString("project_name"));
                    invoiceItem.setPhaseManagerName(dataItem.getString("phase_manager_name"));
                    invoiceItem.setSectionManagerName(dataItem.getString("section_manager_name"));
                    invoiceItem.setProjectManagerName(dataItem.getString("project_manager_name"));
                    invoiceItem.setPhaseManagerPhone(dataItem.getString("phase_manager_phone"));
                    invoiceItem.setSectionManagerPhone(dataItem.getString("section_manager_phone"));
                    invoiceItem.setProjectManagerPhone(dataItem.getString("project_manager_phone"));

                    invoiceItem.setPrice(dataItem.getString("price"));
                    invoiceItem.setRefNumber(dataItem.getString("ref_number"));
                    invoiceItem.setYear(dataItem.getString("year"));
                    invoiceItem.setMonth(dataItem.getString("month"));
                    invoiceItem.setCardNumber(dataItem.getString("card_number"));

                    invoiceItem.setType(dataItem.getInt("type"));
                    invoiceItem.setState(dataItem.getInt("state"));
                    invoiceItem.setGate(dataItem.getInt("gate"));

                    invoiceItems.add(invoiceItem);
                }
                view.invoicesResponse(invoiceItems);
            } else {
                view.serverResponseError();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            view.jsonParseError();
        }
    }*/

    private void parseProfileResponse(String s) {
        try {
            JSONObject object = new JSONObject(s);
            JSONObject status = object.getJSONObject("status");
            int statusCode = status.getInt("code");
            view.isProfileOk(statusCode == 200);
        } catch (JSONException e) {
            e.printStackTrace();
            view.jsonParseError();
        }
    }
}
