package asc.ps.voda.supporter.operator.adpters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import asc.ps.voda.supporter.operator.R;
import asc.ps.voda.supporter.operator.consractors.VisitorListItem;

public class VisitorListAdapter extends RecyclerView.Adapter<VisitorListAdapter.ViewHolder> {

    ArrayList<VisitorListItem> mValues;
    Context mContext;
    ItemListener itemListener;

    public VisitorListAdapter(Context context, ArrayList values, ItemListener itemListener) {
        this.mValues = values;
        this.mContext = context;
        this.itemListener = itemListener;
    }

    public VisitorListAdapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.visitor_list_item, parent, false);
        return new VisitorListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        holder.setData(mValues.get(position));
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public interface ItemListener {
        void onItemClick(VisitorListItem item);
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView txt_behavior_part_1;
        public TextView txt_behavior_part_2;
        public TextView txt_visitor_id;
        public TextView txt_visitor_message;
        public TextView txt_visitor_incoming_message;
        public TextView txt_team_member_id;
        public TextView txt_team_member_message;
        public TextView txt_team_member_incoming_message;
        public TextView txt_my_message;
        public TextView txt_status;
        public TextView txt_time;

        VisitorListItem item;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            txt_behavior_part_1 = itemView.findViewById(R.id.txt_behavior_part_1);
            txt_behavior_part_2 = itemView.findViewById(R.id.txt_behavior_part_2);
            txt_visitor_id = itemView.findViewById(R.id.txt_visitor_id);
            txt_visitor_message = itemView.findViewById(R.id.txt_visitor_message);
        }

        public void setData(VisitorListItem item) {
            txt_behavior_part_1.setText("");
        }

        @Override
        public void onClick(View v) {
            if (itemListener != null) {
                itemListener.onItemClick(item);
            }
        }
    }
}
