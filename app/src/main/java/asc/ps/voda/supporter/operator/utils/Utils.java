package asc.ps.voda.supporter.operator.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Method;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;

import asc.ps.voda.supporter.operator.App;
import asc.ps.voda.supporter.operator.R;

import static android.util.DisplayMetrics.DENSITY_HIGH;
import static android.util.DisplayMetrics.DENSITY_LOW;
import static android.util.DisplayMetrics.DENSITY_MEDIUM;
import static android.util.DisplayMetrics.DENSITY_XHIGH;


public class Utils {
    private static Utils myInstance;
    public final char PERSIAN_COMMA = '،';

    public final char[] arabicIndicDigits = {'٠', '١', '٢', '٣', '٤', '٥',
            '٦', '٧', '٨', '٩'};
    public final String[] firstCharOfDaysOfWeekName = {"ش", "ی", "د", "س",
            "چ", "پ", "ج"};
    public final char[] persianDigits = {'۰', '۱', '۲', '۳', '۴', '۵', '۶',
            '۷', '۸', '۹'};
    private final char[] arabicDigits = {'0', '1', '2', '3', '4', '5', '6',
            '7', '8', '9'};

    public Typeface typeface;


    private Utils() {
    }

    public static Utils getInstance() {
        if (myInstance == null) {
            myInstance = new Utils();
        }
        return myInstance;
    }

    public int getProgramVersionCode(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(context.getPackageName(),
                    "Name not found on Utils.programVersion");
        }
        return 0;
    }

    public String getProgramVersionName(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(context.getPackageName(),
                    "Name not found on Utils.programVersion");
        }
        return "";
    }

    public Typeface getTypeFace(Context cx) {
        if (typeface == null)
            typeface = Typeface.createFromAsset(cx.getAssets(), "fonts/iran_sans_mobile.ttf");//"fonts/NotoNaskhArabic-Regular.ttf");

        return this.typeface;

    }

    public int calculateFontClass1() {
        float dpi = App.context.getResources().getDisplayMetrics().density;
        dpi *= 100;
        if (dpi < DENSITY_LOW) {
            return 10;
        } else if (dpi > DENSITY_LOW && dpi < DENSITY_MEDIUM) {
            return 11;
        } else if (dpi > DENSITY_MEDIUM && dpi < DENSITY_HIGH) {
            return 12;
        } else if (dpi > DENSITY_HIGH && dpi < DENSITY_XHIGH) {
            return 13;
        } else {
            return 14;
        }
    }

    public int calculateFontClass2() {
        float dpi = App.context.getResources().getDisplayMetrics().density;
        dpi *= 100;
        if (dpi < DENSITY_LOW) {
            return 10;
        } else if (dpi > DENSITY_LOW && dpi < DENSITY_MEDIUM) {
            return 12;
        } else if (dpi > DENSITY_MEDIUM && dpi < DENSITY_HIGH) {
            return 14;
        } else if (dpi > DENSITY_HIGH && dpi < DENSITY_XHIGH) {
            return 16;
        } else {
            return 18;
        }
    }

    public void prepareView(View view, int color, int radius) {
        GradientDrawable shape = new GradientDrawable();
        shape.setColor(color);
        shape.setCornerRadius(radius);
        view.setBackground(shape);
    }

    public void prepareTextView(TextView textView) {
        if (typeface == null) {
            typeface = Typeface.createFromAsset(textView.getContext()
                    .getAssets(), "fonts/iran_sans_mobile.ttf");//"fonts/NotoNaskhArabic-Regular.ttf");
        }
        textView.setTypeface(typeface);
        textView.setLineSpacing(0f, 0.8f);
    }

    public void prepareTextView(View view, boolean bold) {
        String tf = "fonts/iran_sans_mobile.ttf";
        if (bold) {
            tf = "fonts/iran_sans_mobile_bold.ttf";
        }
        Typeface typeface = Typeface.createFromAsset(view.getContext()
                .getAssets(), tf);//"fonts/NotoNaskhArabic-Regular.ttf");
        try {
            TextView textView = (TextView) view;
            textView.setTypeface(typeface);
            textView.setLineSpacing(0f, 0.8f);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            EditText editText = (EditText) view;
            editText.setTypeface(typeface);
            editText.setLineSpacing(0f, 0.8f);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            CheckBox checkBox = (CheckBox) view;
            checkBox.setTypeface(typeface);
            checkBox.setLineSpacing(0f, 0.8f);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            RadioButton radioButton = (RadioButton) view;
            radioButton.setTypeface(typeface);
            radioButton.setLineSpacing(0f, 0.8f);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            Button button = (Button) view;
            button.setTypeface(typeface);
            button.setLineSpacing(0f, 0.8f);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public char[] preferredDigits(Context context) {
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        return prefs.getBoolean("PersianDigits", true) ? persianDigits
                : arabicDigits;
    }

    public char[] ArabicDigits(Context context) {
        return arabicDigits;
    }

    public String formatNumber(int number, char[] digits) {
        return formatNumber(Integer.toString(number), digits);
    }

    public String formatNumber(String number, char[] digits) {
        if (digits == arabicDigits)
            return number;
        if (digits == null)
            digits = persianDigits;

        StringBuilder sb = new StringBuilder();
        for (char i : number.toCharArray()) {
            if (Character.isDigit(i)) {
                sb.append(digits[Integer.parseInt(i + "")]);
            } else {
                sb.append(i);
            }
        }
        return sb.toString();
    }

    public String convertDigitToPersian(String str) {

        str = str.replace('0', '۰');
        str = str.replace('1', '۱');
        str = str.replace('2', '۲');
        str = str.replace('3', '۳');
        str = str.replace('4', '۴');
        str = str.replace('5', '۵');
        str = str.replace('6', '۶');
        str = str.replace('7', '۷');
        str = str.replace('8', '۸');
        str = str.replace('9', '۹');

        return str;
    }

    public String PrepareNumberForClock(int number) {
        String param = number + "";
        if (param.length() < 2) {
            param = "0" + param;
        }
        return param;
    }

    public String clockToString(int hour, int minute, char[] digits) {
        return formatNumber(String.format(Locale.ENGLISH, "%d:%02d", hour, minute), digits);
    }

    public void shareTextAs(Context context, String text) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_TEXT, text);
        context.startActivity(Intent.createChooser(i, App.context.getString(R.string.share)));
    }

    public void getPermission(int requsetCode, Activity activity, String[] permission) {
        //todo check this method and improve

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(activity,
                permission[0])
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                    permission[0])) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                ActivityCompat.requestPermissions(activity,
                        permission,
                        requsetCode);
            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(activity,
                        permission,
                        requsetCode);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    public void launchMarket(Context context) {
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + context.getPackageName())));
        } catch (android.content.ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
        }
    }

    public double getDistance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        return (dist);
    }

    public double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    public double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }

    @SuppressLint("HardwareIds")
    public String getDeviceUUID() {
        if (ActivityCompat.checkSelfPermission(App.context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            long mostSig = (Build.BOARD + Build.BRAND + Build.DEVICE + getSerialNumber()).hashCode();
            long minSig = (Build.DEVICE + Build.MANUFACTURER + Build.MODEL + Build.PRODUCT + getImei()[0] + getImei()[1]).hashCode();
            Log.d(App.LOG_TAG, "getDeviceUUID: " + new UUID(mostSig, minSig).toString());
            return new UUID(mostSig, minSig).toString();
        } else {
            String androidId = "" + Settings.Secure.getString(App.context.getContentResolver(), Settings.Secure.ANDROID_ID);
            Log.d(App.LOG_TAG, "getDeviceUUID: ANDROID_ID = " + Settings.Secure.getString(App.context.getContentResolver(), Settings.Secure.ANDROID_ID));
            Log.d(App.LOG_TAG, "getDeviceUUID: WifiMacAddress = " + getWifiMacAddress());
            Log.d(App.LOG_TAG, "getDeviceUUID: " + new UUID(getWifiMacAddress().hashCode(), ((long) androidId.hashCode() << 32) | androidId.hashCode()).toString());
            return new UUID(getWifiMacAddress().hashCode(), ((long) androidId.hashCode() << 32) | androidId.hashCode()).toString();
        }
    }

    public String getInstallUUID() {
        long mostSig = (getDeviceUUID().hashCode());
        long minSig = (App.context.getPackageName()).hashCode();
        Log.d(App.LOG_TAG, "getInstallUUID: " + new UUID(mostSig, minSig).toString());
        return new UUID(mostSig, minSig).toString();
    }

    @SuppressLint("HardwareIds")
    public String getWifiMacAddress() {
        WifiManager wm = (WifiManager) App.context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        assert wm != null;
        return wm.getConnectionInfo().getMacAddress();
    }

    @SuppressLint({"HardwareIds", "MissingPermission"})
    public String[] getImei() {
        if (ActivityCompat.checkSelfPermission(App.context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            String imei2;
            String imei1;
            String device_id1;
            String device_id2;
            String device_id;
            String[] params = {"", ""};

            TelephonyManager tm = (TelephonyManager) App.context.getSystemService(Context.TELEPHONY_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                imei1 = tm != null ? tm.getImei(0) : "1";
                imei2 = tm != null ? tm.getImei(1) : "1";
                params[0] = imei1;
                params[1] = imei2;
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                device_id1 = tm != null ? tm.getDeviceId(0) : "1";
                device_id2 = tm != null ? tm.getDeviceId(1) : "1";
                params[0] = device_id1;
                params[1] = device_id2;
            } else {
                device_id = tm != null ? tm.getDeviceId() : "1";
                params[0] = device_id;
                params[1] = "1";
            }
            return params;
        } else {
            return null;
        }
    }

    /**
     * @return serial number of device (s/n) or empty String it needs READ_PHONE_STATE permission to run correctly
     */
    @SuppressLint("MissingPermission")
    public String getSerialNumber() {
        String serialNumber = "";
        try {
            @SuppressLint("PrivateApi") Class<?> c = Class.forName("android.os.SystemProperties");
            Method get = c.getMethod("get", String.class);

            serialNumber = (String) get.invoke(c, "gsm.sn1");
            assert serialNumber != null;
            if (serialNumber.equals("")) {
                serialNumber = (String) get.invoke(c, "ril.serialnumber");
            }
            assert serialNumber != null;
            if (serialNumber.equals("")) {
                serialNumber = (String) get.invoke(c, "ro.serialno");
            }
            assert serialNumber != null;
            if (serialNumber.equals("")) {
                serialNumber = (String) get.invoke(c, "sys.serialnumber");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        assert serialNumber != null;
        if (serialNumber.equals("") && Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (ActivityCompat.checkSelfPermission(App.context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                serialNumber = Build.getSerial();
            } else {
                return serialNumber;
            }
        }
        if (serialNumber == null) {
            serialNumber = "";
        }
        return serialNumber;
    }

    public boolean isVpnConnected() {
        String iface = "";
        try {
            for (NetworkInterface networkInterface : Collections.list(NetworkInterface.getNetworkInterfaces())) {
                if (networkInterface.isUp())
                    iface = networkInterface.getName();
                Log.d("DEBUG", "IFACE NAME: " + iface);
                if (iface.contains("tun") || iface.contains("ppp") || iface.contains("pptp")) {
                    return true;
                }
            }
        } catch (SocketException e1) {
            e1.printStackTrace();
        }

        return false;
    }

    public String generateRandomString(int length, boolean number, boolean upper, boolean lower) {
        if (!number && !upper && !lower) {
            number = true;
            upper = true;
            lower = true;
        }
        Random random = new Random();
        StringBuilder result = new StringBuilder();
        String characters = "";
        if (number) characters += "0123456789";
        if (lower) characters += "abcdefghijklmnopqrstuvwxyz";
        if (upper) characters += "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        Log.d(App.LOG_TAG, "generateRandomString: characters = " + characters);
        for (int i = 0; i < length; i++) {
            int index = random.nextInt(characters.length());
            result.append(characters.charAt(index));
        }
        return result.toString();
    }

    public String MD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = new byte[0];
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                array = md.digest(md5.getBytes(StandardCharsets.UTF_8));
            } else {
                array = md.digest(md5.getBytes(StandardCharsets.UTF_8));
            }
            StringBuffer sb = new StringBuffer();
            for (byte b : array) {
                sb.append(Integer.toHexString((b & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getFilePath(Context context, Uri uri) throws URISyntaxException {
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;

            try {
                cursor = context.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Turn drawable resource into byte array.
     *
     * @param context parent context
     * @param id      drawable resource id
     * @return byte array
     */
    public byte[] getFileDataFromDrawable(boolean isJPEG, Context context, int id) {
        Drawable drawable = ContextCompat.getDrawable(context, id);
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(isJPEG ? Bitmap.CompressFormat.JPEG : Bitmap.CompressFormat.PNG, 0, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    /**
     * Turn drawable into byte array.
     *
     * @param drawable data
     * @return byte array
     */
    public byte[] getFileDataFromDrawable(boolean isJPEG, Drawable drawable) {
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(isJPEG ? Bitmap.CompressFormat.JPEG : Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public byte[] getFileDataFromDrawable(boolean isJPEG, Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(isJPEG ? Bitmap.CompressFormat.JPEG : Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public String getMoneyFormattedText(String price) {
        return String.format("%,d", Integer.valueOf(price));
    }

    public void hideSoftKeyboard(Activity activity) {
        // Check if no view has focus:
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) App.context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }
}