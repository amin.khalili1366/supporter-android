package asc.ps.voda.supporter.operator.base;

import android.annotation.SuppressLint;
import android.content.pm.ActivityInfo;

import androidx.appcompat.app.AppCompatActivity;

import asc.ps.voda.supporter.operator.App;


public class ActivityBase extends AppCompatActivity {
    public static String TAG = App.LOG_TAG;

    protected void assignViews() {

    }

    protected void prepareViews() {

    }

    protected void setViewFunctions() {

    }

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onResume() {
        super.onResume();
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }
}
