package asc.ps.voda.supporter.operator.activities.login;


import asc.ps.voda.supporter.operator.base.IApi;
import asc.ps.voda.supporter.operator.utils.ApiHelper;

public class ModelLogin implements ContractLogin.model, IApi {

    protected static final int REQUEST_OTP_TYPE = 1;
    protected static final int VALIDATE_OTP_TYPE = 2;
    protected static final int IS_PROFILE_OK_TYPE = 3;
    PresenterLogin presenter;

    public ModelLogin(PresenterLogin presenter) {
        this.presenter = presenter;
    }

    @Override
    public void requestOTP(String phoneNumber) {
        ApiHelper.getInstance().requestOTP(phoneNumber, this, REQUEST_OTP_TYPE);
    }

    @Override
    public void validateOTP(int OTPCode, String userPhone) {
        ApiHelper.getInstance().validateOTP(OTPCode, userPhone, this, VALIDATE_OTP_TYPE);
    }

    @Override
    public void checkProfile() {
        ApiHelper.getInstance().isProfileOk(this, IS_PROFILE_OK_TYPE);
    }

    @Override
    public void onResponse(int type, String s) {
        presenter.onServerResponse(type,s);
    }

    @Override
    public void onError(int type, String s) {
        presenter.onServerError(type,s);
    }

    @Override
    public void onError(int type, String s, int statusCode) {
        presenter.onServerError(type, s, statusCode);
    }

    @Override
    public void onError(int type) {
        presenter.onServerError(type);
    }
}
