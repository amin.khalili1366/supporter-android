package asc.ps.voda.supporter.operator.utils;

import android.text.TextUtils;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

import asc.ps.voda.supporter.operator.App;
import asc.ps.voda.supporter.operator.base.IApi;

public class ApiHelper {
    private static ApiHelper myInstance;

    private ApiHelper() {
    }

    public static ApiHelper getInstance() {
        if (myInstance == null) {
            myInstance = new ApiHelper();
        }
        return myInstance;
    }

    private void callRegularPost(String url, Map<String, String> postParams, IApi connector, int type) {
        if (Utils.getInstance().isNetworkAvailable()) {
            String safeUrl = Statics.getSafeUrl(Statics.getWebServiceUrl() + url);
            Log.d(App.LOG_TAG, "callRegularPost: " + safeUrl);
            StringRequest request = new StringRequest(Request.Method.POST, safeUrl,
                    s -> {
                        Log.d(App.LOG_TAG, url + " ok: " + s);
                        connector.onResponse(type, s);
                    },
                    volleyError -> {
                        Log.d(App.LOG_TAG, url + ": error");
                        String errorMessage = "Unknown error";
                        if (volleyError.networkResponse == null) {
                            if (volleyError.getClass().equals(TimeoutError.class)) {
                                errorMessage = "Request timeout";
                            } else if (volleyError.getClass().equals(NoConnectionError.class)) {
                                errorMessage = "Failed to connect server";
                            }
                            connector.onError(type);
                        } else {
                            String result = new String(volleyError.networkResponse.data);
                            int statusCode = volleyError.networkResponse.statusCode;
                            Log.d(App.LOG_TAG, "on error status code: " + volleyError.networkResponse.statusCode);
                            try {
                                JSONObject response = new JSONObject(result);
                                errorMessage = response.getString("message");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            connector.onError(type, result, statusCode);
                        }
                        Log.i(App.LOG_TAG, errorMessage);
                        volleyError.printStackTrace();
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = postParams;
                    return params;
                }
            };

            int socketTimeout = 30000; // 30 seconds. You can change it
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                    -1,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

            App.getInstance().getRequestQueue().getCache().remove(safeUrl);
            request.setRetryPolicy(policy);
            request.setShouldCache(false);
            App.getInstance().addToRequestQueue(request);
        }
    }

    private void callRegularGet(String url, ArrayList<String> postParams, IApi connector, int type) {
        if (Utils.getInstance().isNetworkAvailable()) {
//            checkIsFcmInstanceOkAndActIfNot();
//            Statics.setInstallSendingToServer(true);
            StringBuilder urlBuilder = new StringBuilder(url);
            for (String item : postParams) {
                urlBuilder.append("/").append(item);
            }
            String safeUrl = Statics.getSafeUrl(Statics.getWebServiceUrl() + urlBuilder.toString());
            Log.d(App.LOG_TAG, "callRegularGet: " + safeUrl);
            StringRequest request = new StringRequest(Request.Method.GET, safeUrl,
                    s -> {
                        Log.d(App.LOG_TAG, url + " ok: " + s);
                        connector.onResponse(type, s);
                    },
                    volleyError -> {
                        Log.d(App.LOG_TAG, url + ": error");
                        String errorMessage = "Unknown error";
                        if (volleyError.networkResponse == null) {
                            if (volleyError.getClass().equals(TimeoutError.class)) {
                                errorMessage = "Request timeout";
                            } else if (volleyError.getClass().equals(NoConnectionError.class)) {
                                errorMessage = "Failed to connect server";
                            }
                            connector.onError(type);
                        } else {
                            String result = new String(volleyError.networkResponse.data);
                            int statusCode = volleyError.networkResponse.statusCode;
                            Log.d(App.LOG_TAG, "on error status code: " + volleyError.networkResponse.statusCode);
                            try {
                                JSONObject response = new JSONObject(result);
                                errorMessage = response.getString("message");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            connector.onError(type, result, statusCode);
                        }
                        Log.i(App.LOG_TAG, errorMessage);
                        volleyError.printStackTrace();
                    });

            int socketTimeout = 30000; // 30 seconds. You can change it
            RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                    -1,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

            App.getInstance().getRequestQueue().getCache().remove(safeUrl);
            request.setRetryPolicy(policy);
            request.setShouldCache(false);
            App.getInstance().addToRequestQueue(request);
        }
    }

    public void checkVersion(IApi connector, int type) {
        Map<String, String> params = new Hashtable<>();
        params.put("app_version", String.valueOf(Utils.getInstance().getProgramVersionCode(App.context)));
//                    params.put("deviceUUID", Utils.getInstance().getDeviceUUID());
//                    params.put("installUUID", Utils.getInstance().getInstallUUID());
//                    params.put("model", Build.MODEL);
//                    params.put("brand", Build.BRAND);
//                    params.put("code_name", Build.DEVICE);
//                    params.put("install_date", Statics.getInstallDate() + "");
//                    if (!Statics.getFcmToken().equals("")) {
//                        params.put("fcm", Statics.getFcmToken());
//                    }
        callRegularPost("application/android/checkVersion", params, connector, type);
    }

    public void checkUserToken(IApi connector, int type) {
        Map<String, String> params = new Hashtable<>();
        params.put("user_token", Statics.getUserToken());
        if (!Statics.getFcmToken().equals("")) {
            params.put("fcm_token", Statics.getFcmToken());
        }
//                    params.put("deviceUUID", Utils.getInstance().getDeviceUUID());
//                    params.put("installUUID", Utils.getInstance().getInstallUUID());
//                    params.put("model", Build.MODEL);
//                    params.put("brand", Build.BRAND);
//                    params.put("code_name", Build.DEVICE);
//                    params.put("install_date", Statics.getInstallDate() + "");
//                    if (!Statics.getFcmToken().equals("")) {
//                        params.put("fcm", Statics.getFcmToken());
//                    }
        callRegularPost("application/android/checkToken", params, connector, type);
    }

    public void requestOTP(String phoneNumber, IApi connector, int type) {
        Map<String, String> params = new Hashtable<>();
        params.put("phone_number", phoneNumber);
//                    params.put("deviceUUID", Utils.getInstance().getDeviceUUID());
//                    params.put("installUUID", Utils.getInstance().getInstallUUID());
//                    params.put("version", String.valueOf(Utils.getInstance().getProgramVersionCode(App.context)));
//                    params.put("version_name", Utils.getInstance().getProgramVersionName(App.context));
//                    params.put("model", Build.MODEL);
//                    params.put("brand", Build.BRAND);
//                    params.put("code_name", Build.DEVICE);
//                    params.put("install_date", Statics.getInstallDate() + "");
//                    if (!Statics.getFcmToken().equals("")) {
//                        params.put("fcm", Statics.getFcmToken());
//                    }
        callRegularPost("application/android/requestOTP", params, connector, type);
    }

    public void validateOTP(final int OTPCode, String userPhone, IApi connector, int type) {
        Map<String, String> params = new Hashtable<>();
        params.put("otp_code", String.valueOf(OTPCode));
        params.put("phone_number", userPhone);

//                    params.put("deviceUUID", Utils.getInstance().getDeviceUUID());
//                    params.put("installUUID", Utils.getInstance().getInstallUUID());
//                    params.put("version", String.valueOf(Utils.getInstance().getProgramVersionCode(App.context)));
//                    params.put("version_name", Utils.getInstance().getProgramVersionName(App.context));
//                    params.put("model", Build.MODEL);
//                    params.put("brand", Build.BRAND);
//                    params.put("code_name", Build.DEVICE);
//                    params.put("install_date", Statics.getInstallDate() + "");
//                    if (!Statics.getFcmToken().equals("")) {
//                        params.put("fcm", Statics.getFcmToken());
//                    }
        callRegularPost("application/android/verifyOTP", params, connector, type);
    }

    public void isProfileOk(IApi connector, int type) {
        ArrayList<String> params = new ArrayList<>();
        params.add(Statics.getUserToken());
//                    params.put("deviceUUID", Utils.getInstance().getDeviceUUID());
//                    params.put("installUUID", Utils.getInstance().getInstallUUID());
//                    params.put("model", Build.MODEL);
//                    params.put("brand", Build.BRAND);
//                    params.put("code_name", Build.DEVICE);
//                    params.put("install_date", Statics.getInstallDate() + "");
//                    if (!Statics.getFcmToken().equals("")) {
//                        params.put("fcm", Statics.getFcmToken());
//                    }
        callRegularGet("application/android/is_profile_ok", params, connector, type);
    }

    public void getProfile(IApi connector, int type) {
        ArrayList<String> params = new ArrayList<>();
        params.add(Statics.getUserToken());
//                    params.put("deviceUUID", Utils.getInstance().getDeviceUUID());
//                    params.put("installUUID", Utils.getInstance().getInstallUUID());
//                    params.put("model", Build.MODEL);
//                    params.put("brand", Build.BRAND);
//                    params.put("code_name", Build.DEVICE);
//                    params.put("install_date", Statics.getInstallDate() + "");
//                    if (!Statics.getFcmToken().equals("")) {
//                        params.put("fcm", Statics.getFcmToken());
//                    }
        callRegularGet("application/android/profile", params, connector, type);
    }

    public void setProfile(IApi connector, int type, String name, String family, String tel, long birthday, int gender, String identityCode, boolean agree) {
        Map<String, String> params = new Hashtable<>();
        params.put("user_token", Statics.getUserToken());
        if (!TextUtils.isEmpty(name)) {
            params.put("name", name);
        }
        if (!TextUtils.isEmpty(family)) {
            params.put("family", family);
        }
        if (!TextUtils.isEmpty(tel)) {
            params.put("tel", tel);
        }
        if (birthday != 0) {
            params.put("birthday", String.valueOf(birthday));
        }
        if (gender != 0) {
            params.put("gender", String.valueOf(gender));
        }
        if (!TextUtils.isEmpty(identityCode)) {
            params.put("identity_code", identityCode);
        }
        if (agree) {
            params.put("agree", "1");
        }
//                    params.put("deviceUUID", Utils.getInstance().getDeviceUUID());
//                    params.put("installUUID", Utils.getInstance().getInstallUUID());
//                    params.put("model", Build.MODEL);
//                    params.put("brand", Build.BRAND);
//                    params.put("code_name", Build.DEVICE);
//                    params.put("install_date", Statics.getInstallDate() + "");
//                    if (!Statics.getFcmToken().equals("")) {
//                        params.put("fcm", Statics.getFcmToken());
//                    }
        callRegularPost("application/android/profile", params, connector, type);
    }
    //---------------------------------------------------------------------------------------------------------
}
