package asc.ps.voda.supporter.operator.activities.splash;

import android.text.TextUtils;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import asc.ps.voda.supporter.operator.App;
import asc.ps.voda.supporter.operator.utils.Statics;

class PresenterSplash implements ContractSplash.presenter {
    private final ContractSplash.view view;

    private final ModelSplash model;

    public PresenterSplash(ContractSplash.view view) {
        this.view = view;
        this.model = new ModelSplash(this);
    }

    @Override
    public void doServerThings() {
        view.tokenIsNotValid();
        //        model.checkVersion();
    }

    @Override
    public void onServerResponse(int type, String s) {
        switch (type) {
            case ModelSplash.checkVersionType:
                Log.d(App.LOG_TAG, "onServerResponse: user token is " + Statics.getUserToken());
                if (TextUtils.isEmpty(Statics.getUserToken())) {
                    Log.d(App.LOG_TAG, "onServerResponse: 1");
                    view.tokenIsNotValid();
                } else {
                    Log.d(App.LOG_TAG, "onServerResponse: 2");
                    model.checkTokenValidation();
                }
                break;
            case ModelSplash.tokenValidationType:
                model.checkProfile();
                break;
            case ModelSplash.isProfileOkType:
                view.profileIsOk();
                break;
            default:
                view.serverResponseError();
        }
    }

    @Override
    public void onServerError(int type, String error, int statusCode) {
        Log.d(App.LOG_TAG, "onServerError: " + error);
        switch (type) {
            case ModelSplash.checkVersionType:
                if (statusCode == 406) {
                    try {
                        JSONObject response = new JSONObject(error);
                        String downloadLink = response.getJSONObject("data").getString("download_link");
                        view.downloadTheLastVersion(downloadLink);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case ModelSplash.tokenValidationType:
                view.tokenIsNotValid();
                break;
            case ModelSplash.isProfileOkType:
                view.profileIsNotOk();
                break;
        }
    }

    @Override
    public void onServerError(int type, String error) {
        Log.d(App.LOG_TAG, "onServerError: ");
        switch (type) {
            case ModelSplash.checkVersionType:
                view.serverResponseError(error);
                break;
            case ModelSplash.tokenValidationType:
                view.tokenIsNotValid();
                break;
            case ModelSplash.isProfileOkType:
                view.profileIsNotOk();
                break;
        }
    }

    @Override
    public void onServerError(int type) {
        Log.d(App.LOG_TAG, "onServerError: ");
        switch (type) {
            case ModelSplash.checkVersionType:
                view.serverResponseError();
                break;
            case ModelSplash.tokenValidationType:
                view.tokenIsNotValid();
                break;
            case ModelSplash.isProfileOkType:
                view.profileIsNotOk();
                break;
        }
    }
}