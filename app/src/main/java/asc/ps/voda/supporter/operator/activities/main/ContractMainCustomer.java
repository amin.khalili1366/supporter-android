package asc.ps.voda.supporter.operator.activities.main;


import java.util.ArrayList;

import asc.ps.voda.supporter.operator.base.BaseContract;
import asc.ps.voda.supporter.operator.consractors.ChatListItem;

public interface ContractMainCustomer {
    interface model extends BaseContract.BaseModel {
        void getPrescriptions();

        void getHistory();
    }

    interface presenter extends BaseContract.BasePresenter {
        void doServerThings();
    }

    interface view extends BaseContract.BaseView {
        void chatListResponse(ArrayList<ChatListItem> historyItems);
    }
}

