package asc.ps.voda.supporter.operator.base;

import android.os.Bundle;

public class ActivityParent extends ActivityBase {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
