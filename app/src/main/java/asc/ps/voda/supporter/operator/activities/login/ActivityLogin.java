package asc.ps.voda.supporter.operator.activities.login;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import asc.ps.voda.supporter.operator.App;
import asc.ps.voda.supporter.operator.activities.main.ActivityMain;
import asc.ps.voda.supporter.operator.base.ActivityParent;
import asc.ps.voda.supporter.operator.databinding.ActivityLoginBinding;
import asc.ps.voda.supporter.operator.utils.Utils;


public class ActivityLogin extends ActivityParent implements ContractLogin.view {

    ActivityLoginBinding root;
    PresenterLogin presenter;
    Handler countDownHandler = new Handler();
    Runnable countDownRunnable;
    private boolean backToExit = false;
    private int resendCodeCounter = 60;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        root = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(root.getRoot());
        presenter = new PresenterLogin(this);
        prepareViews();
        setViewFunctions();
    }

    @Override
    public void onBackPressed() {
        if (backToExit) {
            super.onBackPressed();
        } else {
            backToExit = true;
            Toast.makeText(ActivityLogin.this, "in 3 seconds press back again to exit.", Toast.LENGTH_SHORT).show();
            Handler handler = new Handler();
            handler.postDelayed(() -> backToExit = false, 3000);
        }
    }

    @Override
    protected void prepareViews() {
        super.prepareViews();
        countDownRunnable = new Runnable() {
            @SuppressLint("SetTextI18n")
            @Override
            public void run() {
                if (resendCodeCounter != 0) {
                    resendCodeCounter--;
                    root.txtCounter.setText(resendCodeCounter + " s");
                    countDownHandler.postDelayed(this, 1000);
                } else {
                    root.txtResendOtp.setVisibility(View.VISIBLE);
                }
            }
        };
    }

    @Override
    protected void setViewFunctions() {
        super.setViewFunctions();

        root.btnRequestOtp.setOnClickListener(view -> requestOTP(root.edtPhone.getText().toString()));
        root.btnValidateOtp.setOnClickListener(view -> validateOTP(root.edtOtp.getText().toString(), root.edtPhone.getText().toString()));
        root.btnValidateOtpCancel.setOnClickListener(v -> {
            Utils.getInstance().hideSoftKeyboard(ActivityLogin.this);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("آیا انصراف میدهید؟")
                    .setPositiveButton("بله", (dialog, which) -> {
                        dialog.dismiss();
                        root.edtOtp.setText("");
                        root.llOtp.setVisibility(View.GONE);
                        root.txtResendOtp.setVisibility(View.GONE);
                        root.llPhone.setVisibility(View.VISIBLE);
                    }).setNegativeButton("خیر", (dialog, which) -> dialog.dismiss()).create().show();
        });
        root.txtResendOtp.setOnClickListener(v -> {
            Utils.getInstance().hideSoftKeyboard(this);
            root.txtResendOtp.setVisibility(View.GONE);
            resendCodeCounter = 60;
            presenter.loginStep1(root.edtPhone.getText().toString());
            countDownHandler.postDelayed(countDownRunnable, 1000);
        });
    }


    public String getAppApiToken() {
        SharedPreferences editor = getSharedPreferences("Avanegar", Context.MODE_PRIVATE);
        return editor.getString("AppApiToken", null);
    }

    private void requestOTP(String number) {
        if (!TextUtils.isEmpty(number) && number.startsWith("09") && number.length() == 11) {
            presenter.loginStep1(number);
            countDownHandler.postDelayed(countDownRunnable, 1000);
        } else {
            root.edtPhone.setError("شماره ی تلفن باید یازده کاراکتر باشد و با ۰۹ شروع شود.");
        }
        Utils.getInstance().hideSoftKeyboard(this);
    }

    private void validateOTP(String otp, String number) {
        if (!TextUtils.isEmpty(otp) && !TextUtils.isEmpty(number) && number.startsWith("09") && number.length() == 11) {
            presenter.loginStep2(Integer.parseInt(otp), number);
        } else {
            root.edtOtp.setError("کد پنج رقمی را وارد کنید.");
        }
        Utils.getInstance().hideSoftKeyboard(this);
    }

    @Override
    public void requestOTPSuccessful() {
        root.llPhone.setVisibility(View.GONE);
        root.llOtp.setVisibility(View.VISIBLE);
    }

    @Override
    public void requestOTPDenied() {
        Toast.makeText(this, "خطا در درخواست کد یک بار مصرف.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void validateOTPSuccessful() {
        presenter.checkProfile();
    }

    @Override
    public void validateOTPDenied() {
        Toast.makeText(this, "خطا در راستی آزمایی کد ارسال شده.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void profileOk() {
        Log.d(TAG, "profileOk: ");
        Intent intent;
        intent = new Intent(this, ActivityMain.class);
        ActivityLogin.this.startActivity(intent);
        ActivityLogin.this.finish();
    }

    @Override
    public void profileNotOk() {
        App.shouldGoToMain = true;
//        Intent intent = new Intent(this, ActivityProfile.class);
//        ActivityLogin.this.startActivity(intent);
        ActivityLogin.this.finish();
    }

    @Override
    public void serverResponseError() {
        Toast.makeText(this, "خطا در سرور.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void serverResponseError(String error) {
        Toast.makeText(this, "خطا در سرور.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void serverResponseError(String error, int statusCode) {
        Toast.makeText(this, "خطا در سرور.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void jsonParseError() {
        Toast.makeText(this, "خطا در پردازش پاسخ سرور.", Toast.LENGTH_SHORT).show();
    }
}
