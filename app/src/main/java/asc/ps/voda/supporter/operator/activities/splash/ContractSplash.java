package asc.ps.voda.supporter.operator.activities.splash;

import asc.ps.voda.supporter.operator.base.BaseContract;

interface ContractSplash extends BaseContract {
    interface model extends BaseContract.BaseModel {
        void checkVersion();

        void checkTokenValidation();

        void checkProfile();
    }

    interface view extends BaseContract.BaseView {
        void downloadTheLastVersion(String address);

        void tokenIsNotValid();

        void profileIsOk();

        void profileIsNotOk();
    }

    interface presenter extends BaseContract.BasePresenter {
        void doServerThings();
    }
}
