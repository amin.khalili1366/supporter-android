package asc.ps.voda.supporter.operator.base;

public interface IApi {
    void onResponse(int type, String s);

    void onError(int type, String s, int statusCode);

    void onError(int type, String s);

    void onError(int type);
}
