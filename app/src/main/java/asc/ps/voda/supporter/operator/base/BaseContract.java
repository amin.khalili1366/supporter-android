package asc.ps.voda.supporter.operator.base;

public interface BaseContract {
    interface BaseModel {

    }

    interface BasePresenter {
        void onServerResponse(int type, String s);

        void onServerError(int type, String error, int statusCode);

        void onServerError(int type, String error);

        void onServerError(int type);
    }

    interface BaseView {
        void serverResponseError(String error, int statusCode);

        void serverResponseError(String error);

        void serverResponseError();

        void jsonParseError();
    }
}
