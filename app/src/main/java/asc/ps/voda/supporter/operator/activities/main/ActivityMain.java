package asc.ps.voda.supporter.operator.activities.main;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import androidx.core.view.GravityCompat;
import androidx.recyclerview.widget.GridLayoutManager;

import java.util.ArrayList;

import asc.ps.voda.supporter.operator.R;
import asc.ps.voda.supporter.operator.adpters.ChatListAdapter;
import asc.ps.voda.supporter.operator.base.ActivityParent;
import asc.ps.voda.supporter.operator.consractors.ChatListItem;
import asc.ps.voda.supporter.operator.databinding.ActivityMainBinding;


public class ActivityMain extends ActivityParent implements ContractMainCustomer.view {
    PresenterMainCustomer presenter;
    int _REQUEST_CODE_OTHER_PERMISSIONS = 20;
    private ActivityMainBinding root;
    private boolean backToExit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        root = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(root.getRoot());
        prepareViews();
        setViewFunctions();
        presenter = new PresenterMainCustomer(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.doServerThings();
    }

    @Override
    public void onBackPressed() {
        if (backToExit) {
            super.onBackPressed();
        } else {
                backToExit = true;
                Toast.makeText(ActivityMain.this, "in 3 seconds press back again to exit.", Toast.LENGTH_SHORT).show();
                Handler handler = new Handler();
                handler.postDelayed(() -> backToExit = false, 3000);
        }
    }

    @Override
    protected void prepareViews() {
        super.prepareViews();
        root.appBarActivityMain.contentActivityMain.navigation.setSelectedItemId(R.id.chats);
        setSupportActionBar(root.appBarActivityMain.toolbar);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        //todo helper said that mAppBarConfiguration is not usable
//        AppBarConfiguration mAppBarConfiguration = new AppBarConfiguration.Builder(
//                R.id.nav_profile, R.id.nav_tickets)
//                .setDrawerLayout(root.drawerLayout)
//                .build();
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    protected void setViewFunctions() {
        super.setViewFunctions();
        root.navView.setNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.nav_profile:
//                    startActivity(new Intent(ActivityMain.this, ActivityProfile.class));
                    break;
                case R.id.nav_tickets:
//                    startActivity(new Intent(ActivityMain.this, ActivityTickets.class));
                    break;
                default:
                    Toast.makeText(ActivityMain.this, "default", Toast.LENGTH_SHORT).show();
            }
            return false;
        });

        root.appBarActivityMain.imgMenu.setOnClickListener(v -> {
            if (root.drawerLayout.isDrawerOpen(GravityCompat.START)) {
                root.drawerLayout.closeDrawer(GravityCompat.START);
            } else {
                root.drawerLayout.openDrawer(GravityCompat.START);
            }
        });
    }


    @Override
    public void chatListResponse(ArrayList<ChatListItem> chatListItems) {
        ChatListAdapter chatListAdapter = new ChatListAdapter(this, chatListItems, new ChatListAdapter.ItemListener() {
            @Override
            public void onItemClick(ChatListItem item) {

            }
        });
        root.appBarActivityMain.contentActivityMain.recVChats.setAdapter(chatListAdapter);


//     AutoFitGridLayoutManager that auto fits the cells by the column width defined.
//     AutoFitGridLayoutManager layoutManager = new AutoFitGridLayoutManager(this, 500);
//        recyclerView.setLayoutManager(layoutManager);

        /**
         * Simple GridLayoutManager that spans two columns
         **/
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 1, GridLayoutManager.VERTICAL, false);
        root.appBarActivityMain.contentActivityMain.recVChats.setLayoutManager(gridLayoutManager);
    }

    @Override
    public void serverResponseError() {
        Toast.makeText(this, "خطا", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void serverResponseError(String error, int statusCode) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void serverResponseError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void jsonParseError() {

    }
}