package asc.ps.voda.supporter.operator.adpters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import asc.ps.voda.supporter.operator.R;
import asc.ps.voda.supporter.operator.consractors.ChatItem;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
    ArrayList<ChatItem> mValues;
    Context mContext;
    protected ItemListener mListener;

    public ChatAdapter(Context context, ArrayList values, ItemListener itemListener) {

        mValues = values;
        mContext = context;
        mListener = itemListener;
    }

    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.chat_item, parent, false);
        return new ChatAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        holder.setData(mValues.get(position));
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public interface ItemListener {
        void onItemClick(ChatItem item);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView txt_behavior_part_1;
        public TextView txt_behavior_part_2;
        public TextView txt_visitor_id;
        public TextView txt_visitor_message;
        public TextView txt_visitor_incoming_message;
        public TextView txt_team_member_id;
        public TextView txt_team_member_message;
        public TextView txt_team_member_incoming_message;
        public TextView txt_my_message;
        public TextView txt_status;
        public TextView txt_time;

        ChatItem item;

        public ViewHolder(View v) {
            super(v);
            v.setOnClickListener(this);

            txt_behavior_part_1 = v.findViewById(R.id.txt_behavior_part_1);
            txt_behavior_part_2 = v.findViewById(R.id.txt_behavior_part_2);
            txt_visitor_id = v.findViewById(R.id.txt_visitor_id);
            txt_visitor_message = v.findViewById(R.id.txt_visitor_message);
            txt_visitor_incoming_message = v.findViewById(R.id.txt_visitor_incoming_message);
            txt_team_member_id = v.findViewById(R.id.txt_team_member_id);
            txt_team_member_message = v.findViewById(R.id.txt_team_member_message);
            txt_team_member_incoming_message = v.findViewById(R.id.txt_team_member_incoming_message);
            txt_my_message = v.findViewById(R.id.txt_my_message);
            txt_status = v.findViewById(R.id.txt_status);
            txt_time = v.findViewById(R.id.txt_time);
        }

        public void setData(final ChatItem item) {
            this.item = item;
            txt_behavior_part_1.setText(item.getBehaviorLogPart1());
            txt_behavior_part_2.setText(item.getBehaviorLogPart2());
            txt_visitor_id.setText(item.getVisitorId());
            txt_visitor_message.setText(item.getVisitorMessage());
            txt_visitor_incoming_message.setText(item.getVisitorIncomingMessage());
            txt_team_member_id.setText(item.getTeamMemberId());
            txt_team_member_incoming_message.setText(item.getTeamMemberIncomingMessage());
            txt_team_member_message.setText(item.getTeamMemberMessage());
            txt_my_message.setText(item.getMyMessage());
            txt_my_message.setText(item.isStatusRead() ? "read" : item.isStatusDelivered() ? "delivered" : "send");
            txt_time.setText(String.valueOf(item.getTime()));
        }

        @Override
        public void onClick(View view) {
            if (mListener != null) {
                mListener.onItemClick(item);
            }
        }
    }

}
