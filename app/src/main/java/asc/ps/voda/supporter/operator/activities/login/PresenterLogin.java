package asc.ps.voda.supporter.operator.activities.login;

import org.json.JSONException;
import org.json.JSONObject;

import asc.ps.voda.supporter.operator.utils.Statics;

public class PresenterLogin implements ContractLogin.presenter {

    ContractLogin.model model;
    ContractLogin.view view;

    public PresenterLogin(ContractLogin.view view) {
        this.model = new ModelLogin(this);
        this.view = view;
    }

    @Override
    public void loginStep1(String phoneNumber) {
        //        model.requestOTP(phoneNumber);
        view.requestOTPSuccessful();
    }

    @Override
    public void loginStep2(int OTPCode, String userPhone) {
        //        model.validateOTP(OTPCode, userPhone);
        view.validateOTPSuccessful();
    }

    @Override
    public void checkProfile() {
//        model.checkProfile();
        view.profileOk();
    }

    @Override
    public void onServerResponse(int type, String response) {
        switch (type) {
            case ModelLogin.REQUEST_OTP_TYPE:
                view.requestOTPSuccessful();
                break;
            case ModelLogin.VALIDATE_OTP_TYPE:
                try {
                    JSONObject object = new JSONObject(response);
                    JSONObject data = object.getJSONObject("data");
                    Statics.setUserToken(data.getString("user_token"));
                    Statics.setUserPhone(data.getString("user_phone"));
                    Statics.setUserName(data.getString("user_name"));
                    Statics.setUserFamily(data.getString("user_family"));
                    Statics.setUserFullName(data.getString("user_name") + " " + data.getString("user_family"));
                    view.validateOTPSuccessful();
                } catch (JSONException e) {
                    e.printStackTrace();
                    view.jsonParseError();
                }
                break;
            case ModelLogin.IS_PROFILE_OK_TYPE:
                view.profileOk();
                break;
            default:
        }
    }

    @Override
    public void onServerError(int type,String error, int statusCode) {
        switch (type) {
            case ModelLogin.REQUEST_OTP_TYPE:
                view.requestOTPDenied();
                break;
            case ModelLogin.VALIDATE_OTP_TYPE:
                view.validateOTPDenied();
                break;
            case ModelLogin.IS_PROFILE_OK_TYPE:
                view.profileNotOk();
                break;
            default:
                view.serverResponseError(error);
        }
    }

    @Override
    public void onServerError(int type, String error) {
        switch (type) {
            case ModelLogin.REQUEST_OTP_TYPE:
                view.requestOTPDenied();
                break;
            case ModelLogin.VALIDATE_OTP_TYPE:
                view.validateOTPDenied();
                break;
            case ModelLogin.IS_PROFILE_OK_TYPE:
                view.profileNotOk();
                break;
            default:
                view.serverResponseError(error);
        }
    }

    @Override
    public void onServerError(int type) {
        switch (type) {
            case ModelLogin.REQUEST_OTP_TYPE:
                view.requestOTPDenied();
                break;
            case ModelLogin.VALIDATE_OTP_TYPE:
                view.validateOTPDenied();
                break;
            case ModelLogin.IS_PROFILE_OK_TYPE:
                view.profileNotOk();
                break;
            default:
                view.serverResponseError();
        }
    }
}

