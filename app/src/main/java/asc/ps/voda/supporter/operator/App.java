package asc.ps.voda.supporter.operator;

import android.app.AlarmManager;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;

import java.io.File;

public class App extends Application {

    public static final String LOG_TAG = "support_app";
    public static String mod = "debug"; //debug or release
    public static String DIR_INTERNAL;
    public static String DIR_DB;
    public static App mInstance;
    public static Context context;
    public static SharedPreferences preferences;
    public static AlarmManager alarmManager;
    public static Handler handler;
    public static boolean shouldGoToMain;
    private RequestQueue mRequestQueue;

    public static synchronized App getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        context = getApplicationContext();

        try {
            DIR_INTERNAL = getPackageManager().getPackageInfo(getPackageName(), 0).applicationInfo.dataDir;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        DIR_DB = DIR_INTERNAL + "/db/";
        boolean dir1 = new File(DIR_INTERNAL + "/db").mkdir();
        Log.d(LOG_TAG, "dir1 " + (dir1 ? "created" : "failed"));
        Log.d(App.LOG_TAG, "internal directory = " + DIR_INTERNAL);

        mInstance = this;

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);


        handler = new Handler();
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024); // 1MB cap
            Network network = new BasicNetwork(new HurlStack());
            mRequestQueue = new RequestQueue(cache, network);
            mRequestQueue.start();
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? LOG_TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(LOG_TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
