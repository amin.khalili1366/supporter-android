package asc.ps.voda.supporter.operator.activities.splash;


import asc.ps.voda.supporter.operator.base.IApi;

class ModelSplash implements ContractSplash.model, IApi {

    protected static final int tokenValidationType = 1;
    protected static final int checkVersionType = 2;
    protected static final int isProfileOkType = 3;
    private final PresenterSplash presenter;

    public ModelSplash(PresenterSplash presenter) {
        this.presenter = presenter;
    }

    @Override
    public void checkVersion() {
//        ApiHelper.getInstance().checkVersion(this, checkVersionType);
    }

    @Override
    public void checkTokenValidation() {
//        ApiHelper.getInstance().checkUserToken(this, tokenValidationType);
    }

    @Override
    public void checkProfile() {
//        ApiHelper.getInstance().isProfileOk(this, isProfileOkType);
    }

    @Override
    public void onResponse(int type, String s) {
        presenter.onServerResponse(type, s);
    }

    @Override
    public void onError(int type, String s) {
        presenter.onServerError(type, s);
    }

    @Override
    public void onError(int type, String s, int statusCode) {
        presenter.onServerError(type, s, statusCode);
    }

    @Override
    public void onError(int type) {
        presenter.onServerError(type);
    }
}
