package asc.ps.voda.supporter.operator.activities.splash;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import asc.ps.voda.supporter.operator.App;
import asc.ps.voda.supporter.operator.R;
import asc.ps.voda.supporter.operator.activities.login.ActivityLogin;
import asc.ps.voda.supporter.operator.base.ActivityParent;
import asc.ps.voda.supporter.operator.databinding.ActivitySplashBinding;

public class ActivitySplash extends ActivityParent implements ContractSplash.view {
    private final int animationDuration = 2000;
    private final int nextPageDelay = 2000;
    ActivitySplashBinding root;
    PresenterSplash presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        root = ActivitySplashBinding.inflate(getLayoutInflater());
        setContentView(root.getRoot());

        presenter = new PresenterSplash(this);

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.fade_in_splash);
        animation.setDuration(animationDuration);
        root.imgLogo.setAnimation(animation);
        root.textAppName.setAnimation(animation);
        Handler handler = new Handler();
        handler.postDelayed(() -> presenter.doServerThings(), nextPageDelay);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void profileIsOk() {
        startMainActivity();
    }

    @Override
    public void profileIsNotOk() {
        startProfileActivity();
    }

    @Override
    public void tokenIsNotValid() {
        startLoginActivity();
    }

    @Override
    public void downloadTheLastVersion(String address) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setMessage("your current version is deprecated. please install new one")
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(address));
                        startActivity(intent);
                    }
                })
                .setNegativeButton("not interested", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        ActivitySplash.this.finish();
                    }
                })
                .setCancelable(false)
                .create()
                .show();
    }

    @Override
    public void serverResponseError() {
        alertAndFinish();
    }

    @Override
    public void serverResponseError(String error) {
        alertAndFinish();
    }

    @Override
    public void serverResponseError(String error, int statusCode) {
        alertAndFinish();
    }

    @Override
    public void jsonParseError() {
        alertAndFinish();
    }

    private void startMainActivity() {
//        startActivity(new Intent(this, ActivityMain.class));
        this.finish();
    }

    private void startProfileActivity() {
        App.shouldGoToMain = true;
//        startActivity(new Intent(this, ActivityProfile.class));
        this.finish();
    }

    private void startLoginActivity() {
        startActivity(new Intent(this, ActivityLogin.class));
        this.finish();
    }

    private void alertAndFinish() {
        Toast.makeText(this, "server error. try again in a minute", Toast.LENGTH_SHORT).show();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ActivitySplash.this.finish();
            }
        }, 3000);
    }
}