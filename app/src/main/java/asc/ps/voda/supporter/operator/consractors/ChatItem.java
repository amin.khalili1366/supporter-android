package asc.ps.voda.supporter.operator.consractors;

public class ChatItem {
    long time;
    String behaviorLogPart1, behaviorLogPart2,
            visitorId, visitorMessage, visitorIncomingMessage,
            myMessage,
            teamMemberId, teamMemberMessage, teamMemberIncomingMessage;
    boolean statusDelivered, statusRead;

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getBehaviorLogPart1() {
        return behaviorLogPart1;
    }

    public void setBehaviorLogPart1(String behaviorLogPart1) {
        this.behaviorLogPart1 = behaviorLogPart1;
    }

    public String getBehaviorLogPart2() {
        return behaviorLogPart2;
    }

    public void setBehaviorLogPart2(String behaviorLogPart2) {
        this.behaviorLogPart2 = behaviorLogPart2;
    }

    public String getVisitorId() {
        return visitorId;
    }

    public void setVisitorId(String visitorId) {
        this.visitorId = visitorId;
    }

    public String getVisitorMessage() {
        return visitorMessage;
    }

    public void setVisitorMessage(String visitorMessage) {
        this.visitorMessage = visitorMessage;
    }

    public String getVisitorIncomingMessage() {
        return visitorIncomingMessage;
    }

    public void setVisitorIncomingMessage(String visitorIncomingMessage) {
        this.visitorIncomingMessage = visitorIncomingMessage;
    }

    public String getMyMessage() {
        return myMessage;
    }

    public void setMyMessage(String myMessage) {
        this.myMessage = myMessage;
    }

    public String getTeamMemberId() {
        return teamMemberId;
    }

    public void setTeamMemberId(String teamMemberId) {
        this.teamMemberId = teamMemberId;
    }

    public String getTeamMemberMessage() {
        return teamMemberMessage;
    }

    public void setTeamMemberMessage(String teamMemberMessage) {
        this.teamMemberMessage = teamMemberMessage;
    }

    public String getTeamMemberIncomingMessage() {
        return teamMemberIncomingMessage;
    }

    public void setTeamMemberIncomingMessage(String teamMemberIncomingMessage) {
        this.teamMemberIncomingMessage = teamMemberIncomingMessage;
    }

    public boolean isStatusDelivered() {
        return statusDelivered;
    }

    public void setStatusDelivered(boolean statusDelivered) {
        this.statusDelivered = statusDelivered;
    }

    public boolean isStatusRead() {
        return statusRead;
    }

    public void setStatusRead(boolean statusRead) {
        this.statusRead = statusRead;
    }
}
