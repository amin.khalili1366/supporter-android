package asc.ps.voda.supporter.operator.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;

import androidx.core.app.ActivityCompat;

import java.util.Calendar;
import java.util.Locale;
import java.util.Set;

import asc.ps.voda.supporter.operator.App;


public class Statics {
    public static final String PREF_KEY_LOCALE = "PREF_KEY_LOCALE";

    public static final int PERMISSION_KEY_ALL = 1;

    public static final String[] NEEDED_PERMISSIONS = {
            android.Manifest.permission.RECORD_AUDIO,
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
    };

    private static final String FIRST_USE_PREF_KEY = "FIRST_USE_PREF_KEY";
    private static final String INSTALL_DATE_PREF_KEY = "INSTALL_DATE_PREF_KEY";
    private static final String INSTALL_SENT_TO_SERVER_PREF_KEY = "INSTALL_SENT_TO_SERVER_PREF_KEY";

    private static final String USER_TOKEN_PREF_KEY = "USER_TOKEN_PREF_KEY";
    private static final String USER_NAME_PREF_KEY = "USER_NAME_PREF_KEY";
    private static final String USER_FAMILY_PREF_KEY = "USER_FAMILY_PREF_KEY";
    private static final String USER_FULL_NAME_PREF_KEY = "USER_FULL_NAME_PREF_KEY";
    private static final String USER_PHONE_PREF_KEY = "USER_PHONE_PREF_KEY";
    private static final String FCM_TOKEN_PREF_KEY = "FCM_TOKEN_PREF_KEY";
    private static final String WEB_SOCKET_URL_PREF_KEY = "WEB_SOCKET_URL_PREF_KEY";
    private static final String WEBSERVICE_URL_PREF_KEY = "WEBSERVICE_URL_PREF_KEY";
    private static final String IN_USE_VERSION_PREF_KEY = "IN_USE_VERSION_PREF_KEY";

    public static String getUserToken() {
        return App.preferences.getString(USER_TOKEN_PREF_KEY, "");
    }

    public static void setUserToken(String param) {
        writePreference(USER_TOKEN_PREF_KEY, param);
    }

    public static String getUserName() {
        return App.preferences.getString(USER_NAME_PREF_KEY, "");
    }

    public static void setUserName(String param) {
        writePreference(USER_NAME_PREF_KEY, param);
    }

    public static String getUserFamily() {
        return App.preferences.getString(USER_FAMILY_PREF_KEY, "");
    }

    public static void setUserFamily(String param) {
        writePreference(USER_FAMILY_PREF_KEY, param);
    }

    public static String getUserFullName() {
        return App.preferences.getString(USER_FULL_NAME_PREF_KEY, "");
    }

    public static void setUserFullName(String param) {
        writePreference(USER_FULL_NAME_PREF_KEY, param);
    }

    public static String getUserPhone() {
        return App.preferences.getString(USER_PHONE_PREF_KEY, "");
    }

    public static void setUserPhone(String param) {
        writePreference(USER_PHONE_PREF_KEY, param);
    }

    public static int getInUseVersionKey() {
        //change in every version change
        return App.preferences.getInt(IN_USE_VERSION_PREF_KEY, 0);
    }

    public static void setInUseVersionKey(int param) {
        writePreference(IN_USE_VERSION_PREF_KEY, param);
    }

    public static String getWebServiceUrl() {
        return App.preferences.getString(WEBSERVICE_URL_PREF_KEY, "https://api.tebazdor.ir/v1/");
    }

    public static void setWebServiceUrl(String param) {
        writePreference(WEBSERVICE_URL_PREF_KEY, param);
    }

    public static String getWebSocketUrl() {
        return App.preferences.getString(WEB_SOCKET_URL_PREF_KEY, "");
    }

    public static void setWebSocketUrl(String param) {
        writePreference(WEB_SOCKET_URL_PREF_KEY, param);
    }

    public static String getSafeUrl(String url) {
        url = url.replace("+", "");
        url = url.replace(" ", "%20");
        url = url.replace("%2520", "%20");
        return url;
    }

    public static void setLocal(Activity activity, String languageToLoad) {
        writePreference(PREF_KEY_LOCALE, languageToLoad);
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        activity.getResources().updateConfiguration(config,
                activity.getResources().getDisplayMetrics());
    }

    public static String getLocal() {
        return App.preferences.getString(PREF_KEY_LOCALE, "fa");
    }

    public static void setLocal(Activity activity) {
        String languageToLoad = getLocal();
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        activity.getResources().updateConfiguration(config,
                activity.getResources().getDisplayMetrics());
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean isFirstUse(boolean makeItFirst) {
        boolean answer = isFirstUse();
        if (makeItFirst && answer) {
            writePreference(FIRST_USE_PREF_KEY, false);
        }
        return answer;
    }

    public static boolean isFirstUse() {
        return App.preferences.getBoolean(FIRST_USE_PREF_KEY, true);
    }

    public static void writePreference(String key, boolean value) {
        SharedPreferences.Editor editor = App.preferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static void writePreference(String key, int value) {
        SharedPreferences.Editor editor = App.preferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public static void writePreference(String key, String value) {
        SharedPreferences.Editor editor = App.preferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static void writePreference(String key, Set<String> value) {
        SharedPreferences.Editor editor = App.preferences.edit();
        editor.putStringSet(key, value);
        editor.apply();
    }

    public static void writePreference(String key, long value) {
        SharedPreferences.Editor editor = App.preferences.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    public static void writePreference(String key, float value) {
        SharedPreferences.Editor editor = App.preferences.edit();
        editor.putFloat(key, value);
        editor.apply();
    }

    public boolean isLocationEnabled(Context context) {
        //todo check with other method in this class and make a best
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }
            return locationMode != Settings.Secure.LOCATION_MODE_OFF;
        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    public static boolean isLocationEnabled(Context context, boolean onlyGPSProvider) {
        final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (manager == null) {
            return false;
        }
        if (manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            if (onlyGPSProvider) {
                return false;
            }
        }
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public static Long getInstallDate() {
        return App.preferences.getLong(INSTALL_DATE_PREF_KEY, 0);
    }

    public static void setInstallDate() {
        if (getInstallDate() == 0) {
            writePreference(INSTALL_DATE_PREF_KEY, Calendar.getInstance().getTimeInMillis());
        }
    }

    public static String getFcmToken() {
        final String param = App.preferences.getString(FCM_TOKEN_PREF_KEY, "");
        return param;
    }

    public static void setFcmToken(String token) {
        writePreference(FCM_TOKEN_PREF_KEY, token);
    }
}
