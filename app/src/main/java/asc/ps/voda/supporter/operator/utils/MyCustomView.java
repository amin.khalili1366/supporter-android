package asc.ps.voda.supporter.operator.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;

import androidx.annotation.Nullable;

class MyCustomView extends View {

    float cx = 0f;
    float cy = 0f;
    float radius = 0f;
    float radiusDiff;
    Paint innerPaint = new Paint();
    Paint outerPaint = new Paint();

    private Float recordRadius = 0f;
    private RectF recordRect = new RectF();
    private Paint recordPaint = new Paint();

    private RectF arcRect = new RectF();

    int progress = 0;

    enum Mode {
        Idle, Ready, Recording, Loading;
    }

    Mode mode = Mode.Idle;

    public MyCustomView(Context context) {
        super(context);
    }

    public MyCustomView(Context context, @Nullable @org.jetbrains.annotations.Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (heightMeasureSpec > 0 && widthMeasureSpec > 0) {
            cx = widthMeasureSpec / 2f;
            cy = heightMeasureSpec / 2f;
            radius = Math.min(heightMeasureSpec, widthMeasureSpec) / 2 * 0.95f - getPaddingBottom();

            radiusDiff = (float) dpToPx(10);


            innerPaint.setColor(Color.WHITE);
            innerPaint.setAntiAlias(true);


            outerPaint.setColor(Color.parseColor("#99FFFFFF"));
            outerPaint.setStrokeWidth(radiusDiff);
            outerPaint.setStyle(Paint.Style.FILL);
            outerPaint.setAntiAlias(true);

            recordPaint.setColor(Color.RED);

            recordRadius = radius / 4f;
            recordRect.set(cx - recordRadius, cy - recordRadius, cx + recordRadius, cy + recordRadius);

            arcRect.set(0f, 0f, widthMeasureSpec, heightMeasureSpec);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawCircle(cx, cy, radius, outerPaint);
        canvas.drawCircle(cx, cy, radius - radiusDiff, innerPaint);

        switch (mode) {
            case Ready:
                canvas.drawCircle(cx, cy, radius, outerPaint);
                canvas.drawCircle(cx, cy, radius - radiusDiff, innerPaint);
                canvas.drawCircle(cx, cy, recordRadius, recordPaint);
                break;
            case Recording:
                canvas.drawCircle(cx, cy, radius, outerPaint);
                canvas.drawCircle(cx, cy, radius - radiusDiff, innerPaint);
                canvas.drawRect(recordRect, recordPaint);
                break;
            case Idle:
                canvas.drawCircle(cx, cy, radius, outerPaint);
                canvas.drawCircle(cx, cy, radius - radiusDiff, innerPaint);
                break;
            case Loading:
                canvas.drawArc(arcRect, 0f, 360f * (progress / 100f), true, outerPaint);
                canvas.drawCircle(cx, cy, radius - radiusDiff, innerPaint);
                break;
        }
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
//        return Math.round(dp * displayMetrics.density);
    }

    public int pxToDp(int px) {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        return Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public void setProgress(int progress) {
        this.progress = progress;
        invalidate();
    }
}
