package asc.ps.voda.supporter.operator.activities.login;


import asc.ps.voda.supporter.operator.base.BaseContract;

public interface ContractLogin {
    interface model extends BaseContract.BaseModel {
        void requestOTP(String phoneNumber);

        void validateOTP(int OTPCode, String userPhone);

        void checkProfile();
    }

    interface view extends BaseContract.BaseView {
        void requestOTPSuccessful();

        void requestOTPDenied();

        void validateOTPDenied();

        void validateOTPSuccessful();

        void profileOk();

        void profileNotOk();
    }

    interface presenter extends BaseContract.BasePresenter {
        void loginStep1(String phoneNumber);

        void loginStep2(int OTPCode, String userPhone);

        void checkProfile();
    }
}
