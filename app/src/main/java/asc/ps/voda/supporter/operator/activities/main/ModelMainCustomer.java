package asc.ps.voda.supporter.operator.activities.main;


import asc.ps.voda.supporter.operator.base.IApi;

class ModelMainCustomer implements ContractMainCustomer.model, IApi {
    protected static final int TYPE_HEALTH_DOCUMENTS = 0;
    protected static final int TYPE_WALLET = 2;
    protected static final int TYPE_MAKE_ONLINE_CALL_REQUEST = 4;
    protected static final int TYPE_MY_RESERVES = 6;
    protected static final int TYPE_POSTS = 8;
    protected static final int TYPE_HISTORY = 10;
    protected static final int TYPE_PROJECTS = 12;
    protected static final int TYPE_PHASES = 14;
    protected static final int TYPE_IS_PROFILE_OK = 16;
    private final PresenterMainCustomer presenter;


    public ModelMainCustomer(PresenterMainCustomer presenter) {
        this.presenter = presenter;
    }


    @Override
    public void onResponse(int type, String s) {
        presenter.onServerResponse(type, s);
    }

    @Override
    public void onError(int type, String s) {
        presenter.onServerError(type, s);
    }

    @Override
    public void onError(int type, String s, int statusCode) {
        presenter.onServerError(type, s, statusCode);
    }

    @Override
    public void onError(int type) {
        presenter.onServerError(type);
    }

    @Override
    public void getPrescriptions() {
//        ApiHelper.getInstance().getProperties(this, TYPE_HISTORY);
    }

    @Override
    public void getHistory() {
//        ApiHelper.getInstance().getProjects(this, TYPE_PROJECTS);
    }
}

